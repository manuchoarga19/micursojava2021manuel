package modulo3;

import java.util.Scanner;

public class Ejercicio2
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese un numero entero");
		
		int Numero = scan.nextInt();
		
		if (Numero%2==0)
			System.out.println(" numero par");
		else
			System.out.println(" numero impar");
		
		scan=null;
	}
}
