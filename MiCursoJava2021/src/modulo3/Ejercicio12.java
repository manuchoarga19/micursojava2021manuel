package modulo3;

import java.util.Scanner;

public class Ejercicio12 
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese un numero entero");
		
		int Numero = scan.nextInt();
		
		if (Numero > 0 || Numero < 12)
			
			System.out.println("Pertenece a la primera docena");
		
		else if (Numero > 12 || Numero < 24)
			
			System.out.println("Pertenece a la segunda docena");
		
		else if (Numero > 24 || Numero < 36)
			
			System.out.println("Pertenece a la tercera docena");
		
		else
			System.out.println("El numero " + Numero + " esta fuera de rango");
		
		scan=null;
	}
}
