package modulo3;

import java.util.Scanner;

public class Ejercicio17b 
{
	
	public static void main(String[] args) 
	
	{
			System.out.println("Ingrese el numero que quiere visualizar en la tabla");
			
			Scanner scan = new Scanner(System.in);

			
			int Numero = scan.nextInt();
			
			int SumadeImpares=0;
			
			int SumaTotal=0;
			
			
			System.out.println("la tabla a mostrar es " + Numero);
			
			
			for(int i=1; i<11; i++ )
			{
				int Resultado = Numero * i;
				
				System.out.println(Numero + "x" + i + "=" + Resultado);
				
				int EsPar = Resultado %2;
				
				SumaTotal = SumaTotal + Resultado;
				
				SumadeImpares = SumadeImpares + (Resultado * EsPar);
			}
			
			
			int SumadePares = SumaTotal - SumadeImpares;
			
			System.out.println("Suma de numeros pares = " + SumadePares);
			
			
			scan=null;
	}
}
