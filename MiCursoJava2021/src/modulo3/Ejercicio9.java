package modulo3;


import java.util.Scanner;

public class Ejercicio9
{
	
	public static void main(String[] args)
	{
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Para la competicion los valores son: \n Piedra = 0 \n Papel = 1 \n Tijera = 2\n");
		
		System.out.println("competidor1");
		
		float PrimerCompetidor=scan.nextFloat();
		
		System.out.println("competidor2");
		
		float SegundoCompetidor=scan.nextFloat();
		
		
		if (PrimerCompetidor == SegundoCompetidor)
			
			System.out.println("Empataron");
		else
		{
			if (PrimerCompetidor == 0 && SegundoCompetidor == 2)
				
				System.out.println("Gano el competidor1");
			else if (PrimerCompetidor == 0 && SegundoCompetidor == 1)
				
				System.out.println("Gano el competidor2");
			else if (PrimerCompetidor == 1 && SegundoCompetidor == 0)
				
				System.out.println("Gano el competidor1");
			else if (PrimerCompetidor == 1 && SegundoCompetidor == 2)
				
				System.out.println("Gano el competidor2");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 0)
				
				System.out.println("Gano el competidor2");
			else if (PrimerCompetidor == 2 && SegundoCompetidor == 1)
				
				System.out.println("Gano el competidor1");
		}
		scan=null;
	}
}
