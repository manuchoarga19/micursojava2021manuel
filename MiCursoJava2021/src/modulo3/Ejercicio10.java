package modulo3;

public class Ejercicio10
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese primera variable");
		
		int PrimerValor =scan.nextInt();
		
		System.out.println("Ingrese segunda variable");
		
		int SegundoValor =scan.nextInt();
		
		System.out.println("Ingrese tercera variable");
		
		int TercerValor =scan.nextInt();
		
		if (PrimerValor > SegundoValor && SegundoValor > TercerValor)
			System.out.println("primer variable " + PrimerValor + " es mayor");
		
		else if (SegundoValor > PrimerValor && SegundoValor > TercerValor)
			System.out.println("segunda variable " + SegundoValor + " es mayor");
		
		else if (TercerValor > PrimerValor && TercerValor > SegundoValor)
			System.out.println("tercera variable " + TercerValor + " es mayor");
		
		else
			System.out.println("No hay numero mayor");

		scan=null;
	}
}
